#!/bin/sh

####################################################################################################
# This script will be invoked and executed on Connector host by he-installer after installing Connector
# features and before invoking health check step.

#
# Make sure all the dependent files are present in this directory.
#
# The he-installer passes the following arguments in the same order to this script to use if needed
# $1 => HealthRulesConnectorServer Location
# $2 => Connector distribution location name
# $3 => Username being used to connect to HealthRulesConnectorServer
# $4 => Hostname

##################################################################################################

# Uncomment the following line to invoke providersearch script to copy and configure the buleprints and create indices in Elasticsearch
#./providersearch.sh $@
./createFolder.sh

nacha_ach_count=$(sshpass -p Connector123 ssh -tt -p 8101 -o PasswordAuthentication=no -o StrictHostKeyChecking=no connector@$4 list | grep -c ucare-nacha-extract-bundle)

nacha_ppay_count=$(sshpass -p Connector123 ssh -tt -p 8101 -o PasswordAuthentication=no -o StrictHostKeyChecking=no connector@$4 list | grep -c ucare-nacha-positive-pay-bundle)

he_accum_count=$(sshpass -p Connector123 ssh -tt -p 8101 -o PasswordAuthentication=no -o StrictHostKeyChecking=no connector@$4 list | grep -c he-accumulator-batch-schema-bundle)


if (( $nacha_ach_count > 0 ))
then
    ./ucare_nacha_ach_configure.sh $1 $4 > ~/scripts/ach.log
    echo "ucare-nacha-extract-bundle is installed" $1 $4 > ~/scripts/achcount.log
fi

sleep 10

if (( $nacha_ppay_count > 0 ))
then
    ./ucare_positive_pay_configure.sh $1 $4 > ~/scripts/pp.log
    echo "ucare-nacha-positive-pay-bundle is installed" $1 $4 > ~/scripts/ppcount.log
fi

sleep 10

if (( $he_accum_count > 0 ))
then
   ./generic_accumulator_configure.sh $1 $4 > ~/scripts/heaccum.log
    echo "he-accumulator-batch-schema-bundle is installed" $1 $4 > ~/scripts/heaccumcount.log
fi
